-- File Name: Base.purs
-- Description: Base types
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 06 Apr 2023 19:15:52
-- Last Modified: 02 May 2023 23:09:38

module Chorda.Base where

import Prelude

import Chorda.Key (Key(..), index, naK, naN)

data Quint a b c d e = Quint a b c d e
data Chord = Chord (Quint Key Key Key Key Key)
data IntChord = IntChord (Quint Int Int Int Int Int)

instance showChord :: Show Chord where
  show (Chord (Quint a b c d e))
    | d == naK && e == naK = "(" <> show a <> "," <> show b <> "," <> show c <> ")"
    | e == naK = "(" <> show a <> "," <> show b <> "," <> show c <> "," <> show d <> ")"
    | otherwise = "(" <> show a <> "," <> show b <> "," <> show c <> "," <> show d <> "," <> show e <> ")"

instance showIntChord :: Show IntChord where
  show (IntChord (Quint a b c d e))
    | d == naN && e == naN = "(" <> show a <> "," <> show b <> "," <> show c <> ")"
    | e == naN = "(" <> show a <> "," <> show b <> "," <> show c <> "," <> show d <> ")"
    | otherwise = "(" <> show a <> "," <> show b <> "," <> show c <> "," <> show d <> "," <> show e <> ")"

instance eqChord :: Eq Chord where
  eq t1 t2 = eq t1 t2

instance eqIntChord :: Eq IntChord where
  eq t1 t2 = eq t1 t2

class First t tt where
  fst :: t -> tt

instance chordFirst :: First Chord Key where
  fst (Chord (Quint a _ _ _ _)) = a

instance intChordFirst :: First IntChord Int where
  fst (IntChord (Quint a _ _ _ _)) = a

class Last t tt where
  lst :: t -> tt

instance chordLast :: Last Chord Key where
  lst (Chord (Quint _ _ c d e))
    | d == naK && e == naK = c
    | e == naK = d
    | otherwise = e

instance intChordLast :: Last IntChord Int where
  lst (IntChord (Quint _ _ c d e))
    | d == naN && e == naN = c
    | e == naN = d
    | otherwise = e

class Invert t where
  invert :: t -> t

instance invertChord :: Invert Chord where
  invert (Chord (Quint a b c d e))
    | d == naK && e == naK = Chord (Quint b c a naK naK)
    | e == naK = Chord (Quint b c d a naK)
    | otherwise = Chord (Quint b c d e a)

instance invertIntChord :: Invert IntChord where
  invert (IntChord (Quint a b c d e))
    | d == naN && e == naN = IntChord (Quint b c a naN naN)
    | e == naN = IntChord (Quint b c d a naN)
    | otherwise = IntChord (Quint b c d e a)

class Contains t tt where
  contains :: t -> tt -> Boolean

instance chordContains :: Contains Chord Key where
  contains (Chord (Quint a b c d e)) k
    | d == naK && e == naK = if k == a || k == b || k == c then true else false
    | e == naK = if k == a || k == b || k == c || k == d then true else false
    | otherwise = if k == a || k == b || k == c || k == d || k == e then true else false

instance intChordContains :: Contains IntChord Int where
  contains (IntChord (Quint a b c d e)) k
    | d == naN && e == naN = if k == a || k == b || k == c then true else false
    | e == naN = if k == a || k == b || k == c || k == d then true else false
    | otherwise = if k == a || k == b || k == c || k == d || k == e then true else false

inc :: Int -> Int -> Int
inc current previous
  | current < previous = inc (current + 12) previous
  | otherwise = current

class ToInt t where
  toInt :: t -> IntChord

instance chordToInt :: ToInt Chord where
  toInt (Chord (Quint a b c d e)) = IntChord (Quint aa bb cc dd ee)
    where
    aa = index a
    bb = inc (index b) aa
    cc = inc (index c) bb
    dd = if d == (Key "") then (index d) else inc (index d) cc
    ee = if e == (Key "") then (index e) else inc (index e) dd
