-- File Name: Key.purs
-- Description: Keys
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 06 Apr 2023 08:52:42
-- Last Modified: 23 Nov 2023 10:35:26

module Chorda.Key where

import Prelude

import Data.Array (range)
import Data.Int (ceil, toNumber)

newtype Key = Key String

derive newtype instance eqKey :: Eq Key
instance showKey :: Show Key where
  show (Key a) = a

-- | not a number
naN :: Int
naN = (-9999)

-- | not a key
naK :: Key
naK = Key ""

-- | returns the corresponding key for an index (always sharp)
key :: Int -> Key
key 0 = Key "C"
key 1 = Key "C#"
key 2 = Key "D"
key 3 = Key "D#"
key 4 = Key "E"
key 5 = Key "F"
key 6 = Key "F#"
key 7 = Key "G"
key 8 = Key "G#"
key 9 = Key "A"
key 10 = Key "A#"
key 11 = Key "B"
key x
  | x > 11 = key (x - 12)
  | otherwise = naK

color :: Key -> String
color (Key "Cb") = "white"
color (Key "C") = "white"
color (Key "C#") = "black"
color (Key "D") = "white"
color (Key "D#") = "black"
color (Key "E") = "white"
color (Key "F") = "white"
color (Key "F#") = "black"
color (Key "G") = "white"
color (Key "G#") = "black"
color (Key "A") = "white"
color (Key "A#") = "black"
color (Key "B") = "white"
color (Key "B#") = "white"
color _ = ""

-- | returns the corresponding index for a key
index :: Key -> Int
index (Key "Cb") = 11
index (Key "C") = 0
index (Key "C#") = 1
index (Key "Db") = 1
index (Key "D") = 2
index (Key "D#") = 3
index (Key "Eb") = 3
index (Key "E") = 4
index (Key "Fb") = 4
index (Key "E#") = 5
index (Key "F") = 5
index (Key "F#") = 6
index (Key "Gb") = 6
index (Key "G") = 7
index (Key "G#") = 8
index (Key "Ab") = 8
index (Key "A") = 9
index (Key "A#") = 10
index (Key "Bb") = 10
index (Key "B") = 11
index (Key "B#") = 0
index _ = naN

-- | checks if a given key is valid
isKey :: Key -> Boolean
isKey k
  | index k >= 0 = true
  | otherwise = false

-- | returns the absolute value for integers keys
abs :: Int -> Int
abs x =
  case x >= 0 of
    false -> abs xx
    true -> xx
  where
  xx
    | x >= 0 = x
    | otherwise = 12 - (x * (-1))

-- | offsets an integer key (always in range 0-11)
offset :: Int -> Int -> Int
offset k offs
  | k == naN = k
  | otherwise =
      case t > 11 of
        true -> offset t 0
        false -> abs t
      where
      t
        | k + offs > 11 = k - (12 * (ceil (toNumber k / 12.0))) + offs
        | otherwise = k + offs

-- | creates a range of keys
keyRange :: Int -> Int -> Array Key
keyRange start end
  | start == naN = []
  | end == naN = []
  | otherwise = map (\e -> key $ offset 0 e) (range start end)
