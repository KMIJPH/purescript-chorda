-- File Name: Parse.purs
-- Description: Chord Parsing
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 09 Apr 2023 16:59:14
-- Last Modified: 02 May 2023 23:10:15

module Chorda.Parse where

import Prelude

import Data.Array as A
import Data.CodePoint.Unicode (isLower)
import Data.Int (fromNumber)
import Data.Maybe (Maybe(..))
import Data.Number (fromString)
import Data.String (Pattern(..), codePointFromChar, length, split, splitAt, trim)
import Data.String.Common (toUpper)
import Data.String.Unsafe (char)

type Parsed =
  { key :: String
  , chord :: String
  , inversion :: Int
  }

splitStr :: String -> String -> Array String
splitStr str sep = (Pattern sep) `split` str

hasSignature :: String -> Boolean
hasSignature s =
  if second.before == "#" || second.before == "b" then true else false
  where
  first = splitAt 1 s
  second = splitAt 1 first.after

parseChord :: String -> String
parseChord s =
  case (isLower $ codePointFromChar $ char first.before) of
    true -> "min"
    false -> case hasSignature s of
      true -> if second.after == "" then "maj" else second.after
      false -> if second.before <> second.after == "" then "maj" else second.before <> second.after
  where
  first = splitAt 1 s
  second = splitAt 1 first.after

parseKeyChord :: String -> { key :: String, chord :: String }
parseKeyChord s
  | length s == 0 = { key: "", chord: "" }
  | length s == 1 =
      if (isLower $ codePointFromChar $ char s) then
        { key: (toUpper s), chord: "min" }
      else
        { key: (toUpper s), chord: "maj" }
  | otherwise =
      if hasSignature s then
        { key: (toUpper first.before <> second.before), chord: third }
      else
        { key: toUpper first.before, chord: third }
      where
      first = splitAt 1 s
      second = splitAt 1 first.after
      third = parseChord s

parseInversion :: String -> Int
parseInversion s =
  case fromString s of
    Nothing -> 0
    Just n -> case fromNumber n of
      Nothing -> 0
      Just i -> i

parseCol :: String -> Parsed
parseCol s =
  case A.length a of
    0 -> { key: "", chord: "", inversion: 0 }
    1 -> { key: keychord.key, chord: keychord.chord, inversion: 0 }
    _ -> { key: keychord.key, chord: keychord.chord, inversion: inversion }
  where
  a = splitStr s " "
  filtered = A.filter (\e -> e /= "") a
  trimmed = map (\e -> trim e) filtered
  a0 = trimmed A.!! 0
  a1 = trimmed A.!! 1
  keychord = case a0 of
    Nothing -> { key: "", chord: "" }
    Just ss -> parseKeyChord ss
  inversion = case a1 of
    Nothing -> 0
    Just ss -> parseInversion ss

parse :: String -> Array (Array Parsed)
parse s = map (\r -> map (\c -> parseCol c) r) chords
  where
  rows = splitStr s "\n"
  chords = map (\e -> splitStr e ",") rows
