-- File Name: Chord.purs
-- Description: Chords
-- Wiki: https://en.wikipedia.org/wiki/List_of_chords
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 05 Apr 2023 18:00:02
-- Last Modified: 07 Nov 2023 10:50:28

module Chorda.Chord where

import Prelude

import Chorda.Base (Quint(..), Chord(..), invert)
import Chorda.Key (Key, key, index, offset, naN, naK)
import Data.Maybe (Maybe(..))

-- | creates a chord using integer notation
chord :: Key -> Int -> Int -> Int -> Int -> Chord
chord k b c d e =
  Chord (Quint (key i) (key $ offset i b) (key $ offset i c) dd ee)
  where
  i = index k
  dd = if d /= naN then (key $ offset i d) else naK
  ee = if e /= naN then (key $ offset i e) else naK

-- | inverts a chord n times
invertN :: Int -> Chord -> Chord
invertN 0 c = c
invertN n c = invertN (n - 1) (invert c)

-- | get chord using integer notation
getChord :: String -> Maybe (Key -> Chord)
getChord "sus2" = Just (\k -> chord k 2 7 naN naN)
getChord s | s == "dim" || s == "o" || s == "b5" = Just (\k -> chord k 3 6 naN naN)
getChord s | s == "min" || s == "m" || s == "-" = Just (\k -> chord k 3 7 naN naN)
getChord s | s == "maj" || s == "M" || s == "∆" = Just (\k -> chord k 4 7 naN naN)
getChord s | s == "aug" || s == "+" = Just (\k -> chord k 4 8 naN naN)
getChord s | s == "It+6" || s == "It6" || s == "iv6" = Just (\k -> chord k 4 10 naN naN)
getChord "sus4" = Just (\k -> chord k 5 7 naN naN)
getChord s | s == "o7" || s == "dim7" = Just (\k -> chord k 3 6 9 naN)
getChord s | s == "m7b5" || s == "ø7" = Just (\k -> chord k 3 6 10 naN)
getChord s | s == "oM7" || s == "dimM7" = Just (\k -> chord k 3 6 11 naN)
getChord s | s == "m6" || s == "mM6" = Just (\k -> chord k 3 7 9 naN)
getChord s | s == "-7" || s == "min7" || s == "m7" = Just (\k -> chord k 3 7 10 naN)
getChord s | s == "mM7" || s == "minmaj7" || s == "-∆7" || s == "m∆7" = Just (\k -> chord k 3 7 11 naN)
getChord s | s == "7b5" || s == "Fr+6" || s == "Fr43" = Just (\k -> chord k 4 6 10 naN)
getChord s | s == "6" || s == "M6" = Just (\k -> chord k 4 7 9 naN)
getChord s | s == "7" || s == "Ger+6" || s == "Ger65" = Just (\k -> chord k 4 7 10 naN)
getChord s | s == "maj7" || s == "M7" || s == "∆7" = Just (\k -> chord k 4 7 11 naN)
getChord s | s == "M7#5" || s == "+M7" || s == "augM7" || s == "+∆7" = Just (\k -> chord k 4 8 11 naN)
getChord s | s == "7#5" || s == "+7" || s == "aug7" = Just (\k -> chord k 4 8 10 naN)
getChord "7sus4" = Just (\k -> chord k 5 7 10 naN)
getChord s | s == "min6add9" || s == "m6add9" || s == "m6/9" = Just (\k -> chord k 3 7 9 2)
getChord s | s == "maj9b5" || s == "9b5" = Just (\k -> chord k 4 6 10 2)
getChord s | s == "maj6add9" || s == "6add9" || s == "6/9" = Just (\k -> chord k 4 7 9 2)
getChord "7b9" = Just (\k -> chord k 4 7 10 1)
getChord "9" = Just (\k -> chord k 4 7 10 2)
getChord "7#9" = Just (\k -> chord k 4 7 10 3)
getChord "7#11" = Just (\k -> chord k 4 7 11 6)
getChord s | s == "maj9#5" || s == "9#5" = Just (\k -> chord k 4 8 10 2)
getChord s | s == "maj7#11" || s == "M7#11" || s == "∆7#11" = Just (\k -> chord k 4 8 11 6)
getChord "7/6" = Just (\k -> chord k 4 7 9 10)
getChord "9sus4" = Just (\k -> chord k 5 7 10 2)
getChord _ = Nothing
