-- File Name: UI.purs
-- Description: Html piano
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 06 Apr 2023 15:17:21
-- Last Modified: 23 Nov 2023 10:55:37

module UI where

import Prelude

import Chorda.Base (Chord, contains, toInt, fst, lst)
import Chorda.Chord (invertN, getChord)
import Chorda.Key (color, key, Key(..))
import Chorda.Parse (parse)
import Data.Array (range)
import Data.Foldable (for_)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Console (log)
import Web.DOM.Document as D
import Web.DOM.Element as E
import Web.DOM.Internal.Types (Element)
import Web.DOM.Node as N
import Web.DOM.NodeList as NL
import Web.DOM.NonElementParentNode (getElementById)
import Web.Event.Event (Event)
import Web.HTML (window)
import Web.HTML.HTMLDocument (toDocument)
import Web.HTML.HTMLTextAreaElement as T
import Web.HTML.Window (document)

type Ids =
  { piano_section :: String
  , piano_row :: String
  , piano_chord_cont :: String
  , piano_chord :: String
  , input :: String
  , button :: String
  }

ids :: Ids
ids =
  { piano_section: "chorda-piano-section"
  , piano_row: "chorda-piano-row"
  , piano_chord_cont: "chorda-piano-chord-container"
  , piano_chord: "chorda-piano-chord"
  , input: "chorda-input"
  , button: "chorda-generate"
  }

-- | getElementById wrapper
getElem :: D.Document -> String -> Effect (Maybe Element)
getElem doc id = getElementById id $ D.toNonElementParentNode $ doc

-- | returns the value of an textarea element
textAreaValue :: Maybe Element -> Effect (Maybe String)
textAreaValue elem = do
  case elem of
    Nothing -> pure Nothing
    Just element ->
      case T.fromElement element of
        Nothing -> pure Nothing
        Just textarea -> map Just (T.value textarea)

-- | append a single key to the piano
createKey :: D.Document -> Int -> Chord -> N.Node -> Effect Unit
createKey d i c n = do
  elem <- D.createElement "li" d

  let ints = toInt c
  let k = key i

  case contains ints i of
    true -> E.setAttribute "class" ("key " <> color k <> " active") elem
    false -> E.setAttribute "class" ("key " <> color k) elem

  E.setAttribute "data-key" (show k) elem
  let liNode = E.toNode elem

  span <- D.createElement "span" d
  let spanNode = E.toNode span
  N.setTextContent (" " <> show k) spanNode

  N.appendChild spanNode liNode
  N.appendChild liNode n

-- | creates a single piano chord
createPiano :: D.Document -> Chord -> N.Node -> Effect Unit
createPiano d c n = do
  elem <- D.createElement "ul" d
  E.setAttribute "class" ids.piano_chord elem
  let ulNode = E.toNode elem

  div <- D.createElement "div" d
  E.setAttribute "class" ids.piano_chord_cont div
  let divNode = E.toNode div

  for_ (range start end) \k -> do
    createKey d k c ulNode

  N.appendChild ulNode divNode
  N.appendChild divNode n

  where
  ints = toInt c
  start = if (color $ key (fst ints)) == "white" then (fst ints) else (fst ints) - 1
  end = if (color $ key (lst ints)) == "white" then (lst ints) else (lst ints) + 1

-- | runs the chord thingy
run :: Event -> Effect Unit
run _ = do
  w <- window
  d <- document w
  let doc = toDocument d
  elem <- getElem doc ids.input
  inp <- textAreaValue elem
  container <- getElem doc ids.piano_section

  case container of
    Nothing -> log $ "Couldn't find section" <> ids.piano_section
    Just cont -> do
      let cont_node = E.toNode cont
      children <- N.childNodes cont_node

      child_array <- NL.toArray children
      for_ child_array \n -> do
        N.removeChild n cont_node

      case inp of
        Nothing -> log "Couldn't parse input"
        Just s -> do
          let rows = parse s
          for_ rows \r -> do
            div <- D.createElement "div" doc
            E.setAttribute "class" ids.piano_row div
            let node = E.toNode div

            for_ r \c -> do
              let fun = getChord c.chord
              case fun of
                Nothing -> log $ "Couldn't parse chord " <> (show c)
                Just f -> do
                  let chord = f (Key c.key)
                  let inv = invertN c.inversion chord
                  createPiano doc inv node
                  N.appendChild node cont_node
