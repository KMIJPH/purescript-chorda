-- File Name: Main.purs
-- Description: Simple chord visualizer
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 06 Apr 2023 19:15:52
-- Last Modified: 23 Nov 2023 10:54:59

module Main where

import Prelude

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Exception (throw)
import UI (getElem, ids, run)
import Web.DOM.Element as E
import Web.Event.EventTarget (addEventListener, eventListener)
import Web.HTML (window)
import Web.HTML.Event.EventTypes (click)
import Web.HTML.HTMLDocument (toDocument)
import Web.HTML.Window (document)

main :: Effect Unit
main = do
  w <- window
  d <- document w
  let doc = toDocument d
  gen_button <- getElem doc ids.button

  gen_target <- case gen_button of
    Nothing -> throw $ "No element '" <> ids.button <> "' found"
    Just b -> pure $ E.toEventTarget b

  gen_listener <- eventListener run
  addEventListener click gen_listener true gen_target
