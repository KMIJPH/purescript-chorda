# chorda

## Description

A simple chord generator that can generate piano chords and their inversions.  
The resulting html can be saved by using the built-in screenshot tool provided by the browser.  
A running example can be found [here](https://kmijph.codeberg.page/apps/chorda/).

## Usage

Takes a [comma separated string](./etc/example.txt) as input.  
Each line in the input will create a new line in the output.

```go
// Chord Inversion, Chord Inversion
    C            , Fmaj7    2
    G       1    , Ab7
```

### Implemented chords and aliases

- maj | M | ∆
- min | m | -
- sus2
- dim | o | b5
- aug | +
- It+6 | It6 | iv6
- sus4
- o7 | dim7
- m7b5 | ø7
- oM7 | dimM7
- m6 | mM6
- -7 | min7 | m7
- mM7 | minmaj7 | -∆7 | m∆7
- 7b5 | Fr+6 | Fr43
- 6 | M6
- 7 | Ger+6 | Ger65
- maj7 | M7 | ∆7
- M7#5 | +M7 | augM7 | +∆7
- 7#5 | +7 | aug7
- 7sus4
- min6add9 | m6add9 | m6/9
- maj9b5 | 9b5
- maj6add9 | 6add9 | 6/9
- 7b9
- 9
- 7#9
- 7#11
- maj9#5 | 9#5
- maj7#11 | M7#11 | ∆7#11
- 7/6
- 9sus4

Simple major triads can be denoted with uppercase letters (`C`); minor triads with lowercase letters (`c`).

# Building

```bash
spago bundle
```
