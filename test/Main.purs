-- File Name: Main.purs
-- Description: Grader tests
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 16 Mar 2023 07:24:24
-- Last Modified: 02 May 2023 23:10:57

module Test.Main where

import Prelude

import Chorda.Base (Chord, toInt)
import Chorda.Chord (getChord, invertN)
import Chorda.Key (Key(..), abs, color, index, isKey, key, keyRange, offset)
import Chorda.Parse (hasSignature, parse)
import Data.Maybe (fromJust)
import Effect (Effect)
import Partial.Unsafe (unsafePartial)
import Test.Unit (test)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)

cmaj :: Chord
cmaj =
  f (Key "C")
  where
  f = unsafePartial $ fromJust (getChord "maj")

f7 :: Chord
f7 =
  f (Key "F")
  where
  f = unsafePartial $ fromJust (getChord "7")

csmaj7s11 :: Chord
csmaj7s11 =
  f (Key "C#")
  where
  f = unsafePartial $ fromJust (getChord "maj7#11")

p1 :: String
p1 = "   C, Emaj    2, Fmin 1"

p2 :: String
p2 = "F6 1, Esus4 3\n C#maj 2, Bb9sus4, A7, Bb"

p3 :: String
p3 = "f 1, bb 2"

main :: Effect Unit
main = runTest do
  test "key" do
    equal (key 0) (Key "C")
    equal (key 7) (Key "G")
    equal (key 8) (Key "G#")
    equal (key 12) (Key "C")
    equal (key 14) (Key "D")
    equal (key (-9999)) (Key "")
  test "index" do
    equal (index $ Key "G") 7
    equal (index $ Key "Ab") 8
    equal (index $ Key "ASD") (-9999)
  test "isKey" do
    equal (isKey $ Key "AFD") false
    equal (isKey $ Key "G#") true
    equal (isKey $ Key "Eb") true
  test "abs" do
    equal (abs 0) 0
    equal (abs 7) 7
    equal (abs 100) 100
    equal (abs (-8)) 4
    equal (abs (-24)) 0
  test "offset" do
    equal (offset 0 12) 0
    equal (offset 0 13) 1
    equal (offset 0 24) 0
    equal (offset 0 48) 0
    equal (offset 0 4) 4
    equal (offset 10 4) 2
    equal (offset 11 4) 3
    equal (offset 11 1) 0
    equal (offset 12 8) 8
    equal (offset 20 8) 4
    equal (offset 33 8) 5
    equal (offset (-1) 1) 0
    equal (offset (-3) 5) 2
    equal (offset (-12) 5) 5
    equal (offset (-9999) 5) (-9999)
  test "color" do
    equal (color (Key "C")) "white"
    equal (color (Key "C#")) "black"
    equal (color (Key "F")) "white"
    equal (color (Key "A#")) "black"
  test "keyRange" do
    equal (keyRange 0 5) (map (\e -> Key e) [ "C", "C#", "D", "D#", "E", "F" ])
    equal (keyRange (-2) 5) (map (\e -> Key e) [ "A#", "B", "C", "C#", "D", "D#", "E", "F" ])
    equal (keyRange 0 13) (map (\e -> Key e) [ "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B", "C", "C#" ])
    equal (keyRange 12 24) (map (\e -> Key e) [ "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B", "C" ])
    equal (keyRange 12 (-9999)) []
    equal (keyRange (-9999) 12) []
  test "invertN" do
    equal (show $ invertN 1 cmaj) "(E,G,C)"
    equal (show $ invertN 2 cmaj) "(G,C,E)"
    equal (show $ invertN 3 cmaj) "(C,E,G)"
    equal (show $ invertN 0 f7) "(F,A,C,D#)"
    equal (show $ invertN 1 f7) "(A,C,D#,F)"
    equal (show $ invertN 2 f7) "(C,D#,F,A)"
    equal (show $ invertN 3 f7) "(D#,F,A,C)"
  test "toInt" do
    equal (show $ toInt cmaj) "(0,4,7)"
    equal (show $ toInt $ invertN 1 cmaj) "(4,7,12)"
    equal (show $ toInt $ invertN 2 cmaj) "(7,12,16)"
    equal (show $ toInt f7) "(5,9,12,15)"
    equal (show $ toInt $ invertN 1 f7) "(9,12,15,17)"
    equal (show $ toInt csmaj7s11) "(1,5,9,12,19)"
    equal (show $ toInt $ invertN 1 csmaj7s11) "(5,9,12,19,25)"
  test "hasSignature" do
    equal (hasSignature "C#") true
    equal (hasSignature "C") false
    equal (hasSignature "Gb") true
  test "parse" do
    equal (parse "") [ [ { key: "", chord: "", inversion: 0 } ] ]
    equal (parse p1)
      [ [ { key: "C", chord: "maj", inversion: 0 }
        , { key: "E", chord: "maj", inversion: 2 }
        , { key: "F", chord: "min", inversion: 1 }
        ]
      ]
    equal (parse p2)
      [ [ { key: "F", chord: "6", inversion: 1 }
        , { key: "E", chord: "sus4", inversion: 3 }
        ]
      , [ { key: "C#", chord: "maj", inversion: 2 }
        , { key: "Bb", chord: "9sus4", inversion: 0 }
        , { key: "A", chord: "7", inversion: 0 }
        , { key: "Bb", chord: "maj", inversion: 0 }
        ]
      ]
    equal (parse p3)
      [ [ { key: "F", chord: "min", inversion: 1 }
        , { key: "Bb", chord: "min", inversion: 2 }
        ]
      ]
